export class TodoModel {
  constructor(
    public id: number,
    public name: string,
    public complete: boolean,
    public editing: boolean) {
  }
}
