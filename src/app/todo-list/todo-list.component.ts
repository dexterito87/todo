import {Component} from '@angular/core';
import {TodoModel} from '../models/todo.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent {
  todoTitle = '';
  idForTodo = 4;
  todos: TodoModel[] = [
    new TodoModel(1, 'Много Думи', false, false),
    new TodoModel(2, 'Задача 1', false, false),
    new TodoModel(3, 'Задача 2', false, false)
  ];

// ADD To-do//


  addTodo(): void {
    if (this.todoTitle.trim().length === 0) {
      return;
    }


    this.todos.push({
      id: this.idForTodo,
      name: this.todoTitle,
      complete: false,
      editing: false

    });
    console.log(this.todos);
    this.todoTitle = '';
    this.idForTodo++;
  }

  // edit todo//

  editTodo(todo: TodoModel): void {
    todo.editing = true;
  }

  doneEdit(todo: TodoModel): void {
    todo.editing = false;
  }

  // Delete Todo //

  deleteTodo(id: number): void {
    this.todos = this.todos.filter(asd => asd.id !== id);
  }

  // checkbox //

  onCheckBoxChange(data: any, id: number): void {
    if (id < this.todos.length) {
      this.todos[id].complete = data.target.checked;
    }
  }
}
